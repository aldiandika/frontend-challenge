import axios from "axios";
import { Component } from "react";
import AddrListItem from "../components/addr-list-item";
import Panel from "../components/panel-component";
import UserListItem from "../components/user-list-item";
import "../styles/default-style.css";

import IconImg from "../__mocks__/icon-asset";

class Mainpage extends Component {
  state = {
    userData: [],
  }

  componentDidMount() {
    axios({
      method: "get",
      url: `https://randomuser.me/api/?results=25`,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    }).then(
      res => {
        // console.log(res.data);
        let rawData = res.data.results;
        console.log(rawData);
        this.setState({
          userData: rawData
        })
      }
    ).catch(
      err => {
        console.log(err)
      }
    )

  }
  render() {



    return (
      <>
        <div className="root-container">
          <div className="card-container">
            <h4>Member Dashboard</h4>

            {/* Panel */}
            <div className="grid-container">
              <Panel
                title="10"
                subtitle="Different Nationality"
                icon={IconImg[0]}
              />
              <Panel
                title="Female"
                subtitle="Most gender"
                icon={IconImg[2]}
              />
              <Panel
                title="~40"
                subtitle="Average Age"
                icon={IconImg[3]}
              />
              <Panel
                title="~10 year"
                subtitle="Average Membership"
                icon={IconImg[4]}
              />
            </div>

            {/* Table  */}
            <div className="tables-container">
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  justifyContent: "flex-start"
                }}
              >
                {this.state.userData.map((data, index) => (
                  <>
                    <div
                      style={{
                        width: "100%",
                        height: "100%",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "flex-start"
                      }}
                    >
                      <UserListItem
                        key={index}
                        userData={data}
                        country={data.location.country}
                      />
                      <AddrListItem
                        key={index}
                        addr={data.location}
                      />
                    </div>


                  </>
                ))}
              </div>

            </div>
          </div>
        </div>
      </>
    )
  }
}
export default Mainpage;