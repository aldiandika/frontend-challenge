import {
  faFlag,
  faMale,
  faFemale,
  faWalking,
  faPeopleGroup
} from "@fortawesome/free-solid-svg-icons";

const IconImg = [
  faFlag,
  faMale,
  faFemale,
  faWalking,
  faPeopleGroup
]

export default IconImg;