
const AddrListItem = ({ addr }) => {
  return (
    <div
      style={{
        height: "100%",
        width: "50%",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingLeft: "100px",
        fontWeight: 700
      }}
    >
      {addr.street.name + " " + addr.street.number + ", " + addr.country}
    </div>
  )
}
export default AddrListItem;