import { useState, useEffect } from "react";

const UserListItem = ({
  userData,
  country
}) => {
  // useState
  // function getKeyByValue(object, value) {
  //   return Object.keys(object).find(key => object[key] === value);
  // }

  // useEffect(() => {
  //   axios({
  //     method: "get",
  //     url: `https://flagcdn.com/en/codes.json`,
  //     headers: {
  //       "Content-Type": "application/json",
  //       "Access-Control-Allow-Origin": "*"
  //     }
  //   }).then(
  //     res => {
  //       // console.log(res.data);
  //     }
  //   ).catch(
  //     err => {
  //       console.log(err)
  //     }
  //   )
  // })

  return (
    <>
      <div
        style={{
          width: "50%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingTop: "8px",
          paddingBottom: "8px"
        }}
      >
        <div style={{ paddingRight: 12, paddingLeft: 12 }}>
          <img src={userData.picture.thumbnail} alt="Avatar" class="avatar" />
        </div>

        <div
          style={{
            width: "250px",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            paddingRight: 12,
            paddingLeft: 12
          }}
        >
          <div style={{ fontWeight: 700, fontSize: "0,5em" }}> {userData.name.first + " " + userData.name.last}</div>
          <div style={{ color: "grey", fontSize: "16px" }}>{userData.email}</div>
        </div>
        <div style={{ width: "50px", fontWeight: 700, fontSize: "0,5em" }}>
          {userData.dob.age}
        </div>
        <div style={{ width: "50px" }}>
          {userData.gender === "male" ? (<>
            <div className="box-gender-male">Male</div>
          </>) : (<>
            <div className="box-gender-female">Female</div>
          </>)}
        </div>
        <div>
          <img
            src="https://flagcdn.com/w20/az.png"
            srcset="https://flagcdn.com/w40/az.png 2x"
            width="20"
            alt="Azerbaijan" />
        </div>
      </div>
    </>
  )
}

export default UserListItem;