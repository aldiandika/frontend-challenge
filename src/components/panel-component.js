import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Panel = ({
  title,
  subtitle,
  icon
}) => {
  return (
    <>
      <div className="panel-box">
        <div className="panel">
          <div className="sub-panel-left">
            <div
              style={{
                paddingLeft: "0.4em",
                fontSize: "1.5em",
                fontWeight: 500
              }}>{title}</div>
            <div
              style={{
                paddingTop: "0.2em",
                paddingLeft: "0.8em",
                fontSize: "0.8em",
                color: "grey"
              }}>{subtitle}</div>
          </div>

          <div className="sub-panel-right">
            <FontAwesomeIcon icon={icon} style={{ width: "50px", height: "50px" }} />
          </div>
        </div>

      </div>
    </>
  )
}

export default Panel;